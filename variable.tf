## vpc 
variable "vpc-cidr" {
  type        = string
  description = "need to provide vpc cidr"
}

variable "vpc-tags" {
  type        = map(string)
  description = "map valid tags"
}


###public subnet
variable "pubsub1" {
  type        = string
  description = "need to provide vpc cidr"
}

variable "pubSub1-tag" {
  type        = map(string)
  description = "map valid tags"
}


###private subnet1
variable "pvtsub1" {
  type        = string
  description = "need to provide vpc cidr"
}

variable "pvtsub1-tag" {
  type        = map(string)
  description = "map valid tags"
}


###private subnet2 
variable "pvtsub2" {
  type        = string
  description = "need to provide vpc cidr"
}

variable "pvtsub2-tag" {
  type        = map(string)
  description = "map valid tags"
}


### internet gateway
variable "igw-tags" {
  type        = any
  default     = {}
  description = "internet gateway tags"
}

### public route table tags
variable "public-rt" {
  type        = map(string)
  default     = {}
  description = "rt tags"
}

### private route table tags
variable "private-rt" {
  type        = map(string)
  default     = {}
  description = "rt tags"
}


variable "nat-gateway" {
  default     = {}
  description = "nat gatway tags "
}

###### Private Security group

variable "sg_pvt" {
  type        = string
  description = "private security group"
  default     = "Sg-pvt"
}

variable "https_port" {
  type        = string
  description = "TCP port for https"
  default     = "443"
}
variable "ssh_port" {
  type        = string
  description = "TCP port for ssh"
  default     = "22"
}
variable "http_port" {
  type        = string
  description = "public"
  default     = "80"
}

variable "pvt_sg_tags" {
  default     = {}
  description = "Prvate Security group tags "
  type        = map(string)
}
variable "pb_sg_tags" {
  default     = {}
  description = "Public Security group tags for OpenSearch"
  type        = map(string)
}

variable "sg_pb" {
  type        = string
  description = "public security group"
  default     = "public-sg"
}
##------------------------------------------------##
#for instances
variable "ami" {
  type        = string
  description = "ami id for "
  # default     = {}
}
variable "pub-pc-tags" {
  type        = map(string)
  description = "tags for public instance"
  default     = {} 
}

variable "pvt-pc1-tags" {
  type        = map(string)
  description = "tags for public instance"
  default     = {} 
}

