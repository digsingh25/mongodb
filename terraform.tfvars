### VPC CIDR
vpc-cidr = "10.10.0.0/17"
vpc-tags = {
  Name = "myVpc"
  type = "ubuntu"
}

### Public Subnet1
pubsub1 = "10.10.32.0/19"
pubSub1-tag = {
  Name = "pubSubnet1"
  type = "public1"
}

### Private Subnet1
pvtsub1 = "10.10.64.0/20"
pvtsub1-tag = {
  Name = "privateSubnet1"
  type = "private-AZ-1"
}

### Private Subnet2
pvtsub2 = "10.10.80.0/20"
pvtsub2-tag = {
  Name = "privateSubnet2"
  type = "private-AZ-2"
}

### internet gateway
igw-tags = {
  Name = "internetGT"
  type = "igw"
}

### public route table tags
public-rt = {
  Name = "PublicRt"
  type = "public"
}

### private route table tags
private-rt = {
  Name = "PrivateRt"
  type = "private"
}

### NAT gateway tags
nat-gateway = {
  Name  = "nat-gatway"
  onwer = "digvijay"
}
### private security group tags details
pvt_sg_tags = {
  Name    = "private-sg"
  Owner   = "digvijay"
  purpose = "pvt-sg"
}

pb_sg_tags = {
  Name    = "public-sg"
  Owner   = "digvijay"
  purpose = "public security group"
}
### for Instance ----------------------------

    ami     = "ami-0150a25ca6e1d4d51"
    pub-pc-tags  = {
    Name    = "public-pc",
    Owner   = "digvijay",
    family = "ubuntu"
}

 pvt-pc1-tags  = {
    Name    = "Private-pc1",
    Owner   = "digvijay",
    family = "ubuntu"
}

