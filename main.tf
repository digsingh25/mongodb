## provider
provider "aws" {
  region = "ap-south-1"
}

## Create a VPC
resource "aws_vpc" "vpcMain" {
  cidr_block = var.vpc-cidr
  tags       = var.vpc-tags
}

## Create a publicsubnet Subnet
resource "aws_subnet" "pubsub1" {
  vpc_id                  = aws_vpc.vpcMain.id
  cidr_block              = var.pubsub1
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]
  tags                    = var.pubSub1-tag

}

## Create a Prviate Subnet1
resource "aws_subnet" "pvtsub1" {
  vpc_id                  = aws_vpc.vpcMain.id
  cidr_block              = var.pvtsub1
  tags                    = var.pvtsub1-tag
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[1]

}

## Create a Prviate Subnet2
resource "aws_subnet" "pvtsub2" {
  vpc_id                  = aws_vpc.vpcMain.id
  cidr_block              = var.pvtsub2
  tags                    = var.pvtsub2-tag
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[2]

}
## Create a internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpcMain.id
  tags   = var.igw-tags
}

### aws_eip
resource "aws_eip" "elasticIP" {

}

### NAT Gateway object 
resource "aws_nat_gateway" "nat-gateway" {
  connectivity_type = "public"
  allocation_id     = aws_eip.elasticIP.id
  subnet_id         = aws_subnet.pubsub1.id
  depends_on        = [aws_internet_gateway.igw]
  tags              = var.nat-gateway
}

### Create a public route table
resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.vpcMain.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = var.public-rt
}

## Create a private route table
resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.vpcMain.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat-gateway.id
  }
  tags = var.private-rt
}

### Associate Public Route Table to Public Subnets

resource "aws_route_table_association" "pubrtas1" {
  subnet_id      = aws_subnet.pubsub1.id
  route_table_id = aws_route_table.public-rt.id
}

### Associate Private Route Table to Private Subnets
resource "aws_route_table_association" "prirtas1" {
  subnet_id      = aws_subnet.pvtsub2.id
  route_table_id = aws_route_table.private-rt.id

}
resource "aws_route_table_association" "prirtas2" {
  subnet_id      = aws_subnet.pvtsub1.id
  route_table_id = aws_route_table.private-rt.id

}

#### Aws Security group for private

resource "aws_security_group" "private_security_group" {
  name        = var.sg_pvt
  description = "Private security group"
  vpc_id      = aws_vpc.vpcMain.id

  ingress {
    description = "Traffic from VPC"
    from_port   = var.https_port
    to_port     = var.https_port
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.vpcMain.cidr_block]
  }
  ingress {
    description = "Traffic from VPC"
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.vpcMain.cidr_block]
  }

  ingress {
    description = "Traffic from public"
    from_port   = var.http_port
    to_port     = var.http_port
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.vpcMain.cidr_block]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = var.pvt_sg_tags
}

#### Aws Security group for public
resource "aws_security_group" "public_security_group" {
  name        = var.sg_pb
  description = "security group of os engine"
  vpc_id      = aws_vpc.vpcMain.id

  ingress {
    description = "Traffic from VPC"
    from_port   = var.https_port
    to_port     = var.https_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Traffic from VPC"
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Traffic from public "
    from_port   = var.http_port
    to_port     = var.http_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = var.pb_sg_tags
}

### Create a key pair
##-------------------------------------------------##
resource "tls_private_key" "rsa" {
  algorithm          = "RSA"
  rsa_bits           = 4096
}

resource "aws_key_pair" "tf_key" {
  key_name           = "tf_key"
  public_key         = tls_private_key.rsa.public_key_openssh
}
resource "local_file" "tf_key" {
  content           = tls_private_key.rsa.private_key_pem
  filename          = "/home/digvijay/Downloads/tf_key.pem"
  file_permission   = "0400"
}


### Create Instance 
##-------------------------------------------------##

resource "aws_instance" "pub-pc" {
  ami                    = var.ami
  instance_type          = "t2.large"
  key_name               = "tf_key"
  subnet_id              = aws_subnet.pubsub1.id
  vpc_security_group_ids = [aws_security_group.public_security_group.id]
  
  tags                   = var.pub-pc-tags
}

resource "aws_instance" "pvt-pc1" {
  ami                    = var.ami
  instance_type          = "t2.large"
  key_name               = "tf_key"
  subnet_id              = aws_subnet.pvtsub1.id
  vpc_security_group_ids = [aws_security_group.private_security_group.id]
  
  tags                   = var.pvt-pc1-tags
}




