output "vpc-output" {
  value = aws_vpc.vpcMain.id
}

output "sub-pub1-output" {
  value = aws_subnet.pubsub1.id
}

output "pvtsub1-output" {
  value = aws_subnet.pvtsub1.id
}

output "pvtsub2-output" {
  value = aws_subnet.pvtsub2.id
}

output "aws_internet_gateway" {
  value = aws_internet_gateway.igw.id
}

output "public-rt-output" {
  value = aws_route_table.public-rt.id
}

output "Private-rt-output" {
  value = aws_route_table.private-rt.id
}

output "NATgw-output" {
  value = aws_nat_gateway.nat-gateway.id

}

output "aws_eip-output" {
  value = aws_eip.elasticIP
}

output "private-sg-id" {
  value = aws_security_group.private_security_group.id
}

output "public-sg-id" {
  value = aws_security_group.public_security_group.id
}


